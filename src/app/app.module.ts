import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { TodoComponent } from './todo/todo.component';
import { TodosComponent } from './todos/todos.component';
import { RegisterComponent } from './register/register.component';
import { NavComponent } from './nav/nav.component';

import { Routes, RouterModule } from '@angular/router'
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    TodoComponent,
    TodosComponent,
    RegisterComponent,
    NavComponent,

  ],
  imports: [
    BrowserModule,
    FormsModule,
    RouterModule.forRoot([ // נתיבים לאתר
   
      {path:'register', component:RegisterComponent},
      {path:'todos', component:TodosComponent},
      {path:'**', component:RegisterComponent}  // אם מכניסים משהו שונה תעביר ל.. תמיד להכניס אחרון
    ])


  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
