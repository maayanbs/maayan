import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.css']
})
export class TodoComponent implements OnInit {
  @Input() data:any;
  @Output() yosi = new EventEmitter<any>();

id;
title:string;
price;
showit(){
  this.yosi.emit(this.title);
}

  constructor() { }

  ngOnInit() {
  this.id= this.data.id;
  this.title= this.data.title;
  this.price= this.data.price;
  }

}
